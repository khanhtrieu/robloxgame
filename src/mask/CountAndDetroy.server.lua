local mask = script.Parent

--[[
@description count the mask and remove the mask when the player touched	
--]]
local function addMask(otherPart)
	local player = game.Players:FindFirstChild(otherPart.Parent.Name)
	if player then
		player.leaderstats.Mask.Value = player.leaderstats.Mask.Value + 1
		mask:Destroy()
	end
end


mask.Touched:Connect(addMask)