local DataStoreService = game:GetService("DataStoreService")
local playerData = DataStoreService:GetDataStore("PlayerData")

--[[
@description Load data total mask and total vaccine to show on screen	
--]]
local function onPlayerJoin(player)
	local leaderstats = Instance.new("Folder")
	leaderstats.Name='leaderstats'
	leaderstats.Parent = player
	
	local mask = Instance.new("IntValue")
	mask.Name="Mask"
	mask.Parent = leaderstats
	
	local vaccine = Instance.new("IntValue")
	vaccine.Name="Vaccine"
	vaccine.Parent=leaderstats
	
	local playerUserId = 'Player_'..player.UserId
	local data = playerData:GetAsync(playerUserId)
	if data  then
		mask.Value =  0
		--data['Money']
		vaccine.Value = 0
		--data['Vaccine']
	else
		mask.Value = 0
		vaccine.Value = 0
	end
end

--[[
@description create table to save total mask and total vaccine the player collected
--]]
local function create_table(player)
	local player_stats = {}
	for _,stat in pairs(player.leaderstats:GetChildren()) do
		player_stats[stat.Name] = stat.Value
	end
	return player_stats
end

--[[
@description Create json data to save total mask and total vaccine of the player when game exit
--]]
local function onPlayerExit(player)
	local player_stats = create_table(player)
	local success, err = pcall(function()
		local playerUserId = 'Player_'..player.UserId
		playerData:SetAsync(playerUserId, player_stats)
	end)
	if not success then
		warn('Could not save data')
	end
end

game.Players.PlayerAdded:Connect(onPlayerJoin)
game.Players.PlayerRemoving:Connect(onPlayerExit)
