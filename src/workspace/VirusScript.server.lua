local player = game.Players.LocalPlayer
local csBuilding = game.Workspace.CSEBuilding

local Players = game:GetService("Players")
--[[
Set player respawn on specific location
--]]
local function onPlayerAdded(other)
	other.RespawnLocation  = game.Workspace.SpawnLocation
end
Players.PlayerAdded:Connect(onPlayerAdded)



--[[-
@description This script automatic copy model virus and random poistion the copy
-]]

local function generateVirus()
    local totalVirus = 1;
    local rootVirus = game.Workspace.virus
    local rdRootX = math.random(-45,28)
    local rdRootZ = math.random(356,434)
    --rootVirus:MoveTo(Vector3.new(rdRootX,4.3,rdRootZ)) 
    for count = 1,  totalVirus do
        local cpVirus = rootVirus:Clone()
        cpVirus.Parent = game.Workspace
        local xPos = math.random(-36,29.5)
        local zPos = math.random(17,247)
        cpVirus:MoveTo(Vector3.new(xPos,4.3,zPos)) 
    end
end


--[[-
@description This script trigger when player hit virus then system will check total vaccine to make decision for player die or continue
-]]
local function killPlayer(otherPart)
    local player = game.Players:FindFirstChild(otherPart.Parent.Name)
    if player then
        local totalVaccine = player.leaderstats.Vaccine.Value
        if totalVaccine == 0 then
            local partParent = otherPart.Parent
            local humanoid  = partParent:FindFirstChild("Humanoid")
            if humanoid  then
                player.RespawnLocation  = game.Workspace.SpawnLocation
                player.leaderstats.Mask.Value = 0;
                player.leaderstats.Vaccine.Value = 0;
                humanoid.Health = 0
                --player:LoadCharacter()
                
                
                
            end
        end
        
    end
end

local function touchCSEBuilding(otherPart)
    local player = game.Players:FindFirstChild(otherPart.Parent.Name)
    if player then
        local totalVaccine = player.leaderstats.Vaccine.Value
        local totalMask = player.leaderstats.Mask.Value
        if totalVaccine > 0 and totalMask > 0 then
            local winGui = player.PlayerGui:WaitForChild('WinGui')
            csBuilding:remove()
            --player.leaderstats.Mask.Value = 0;
            --player.leaderstats.Vaccine.Value = 0;
            winGui.Enabled =true
            --local winGui = player.PlayerGui.WinGui
            --local label =winGui.WinFrame
            --label.Visible = true
        else
            local notificationGui = player.PlayerGui:WaitForChild('notificationGui')
            
            local textMsg = "Oops! You cannot enter the building without a mask and a vaccine"
            if(totalVaccine == 0) then
                textMsg = "Oops! You cannot enter the building without a vaccine"
            elseif (totalMask == 0) then
                textMsg = "Oops! You cannot enter the building without a mask"
            end
            notificationGui.gameStatusPopup.Text = textMsg
            notificationGui.gameStatusPopup.TextTransparency = 0;
            wait(2)
            notificationGui.gameStatusPopup.TextTransparency = 1;
        end
    end
end

--[[
@description  assign function killPlayer() to virus children model and part  
--]]
local function setupActionTouchIntoVirusModel()
    local parent = game.Workspace
    for k,v in pairs(parent:GetDescendants()) do
        if v.Name == "virus" then
            local virus = v;
            
            
            for index,descendant in pairs(virus:GetDescendants()) do
                for _, child in pairs((descendant:GetChildren())) do
                    if child:IsA("Part") then
                        child.Touched:Connect(killPlayer)
                    end
                end

            end
        elseif v.Name == "CSEBuilding" then
            local csebuilding = v;
            for index,descendant in pairs(csebuilding:GetDescendants()) do
                if descendant:IsA("Part") then
                    descendant.Touched:Connect( touchCSEBuilding)
                end
            end
        end
    end
end

local function generateMaskPosition()
    local mask = game.Workspace.Mask
    local xPos = math.random(-48,31)
    local zPos = math.random(478,605)
    mask.Position = Vector3.new(xPos,4.3,zPos)

    --[[
    Generate multiple mask    
    --]]
    for i =  1, 50 do
        local randX = math.random(-232,68)
        local randZ = math.random(-8.8,652)
        local maskClone =mask:Clone();
        maskClone.Parent= game.Workspace
        maskClone.Position  = Vector3.new(randX,100,randZ)
    end 

end

--[[
Reset Mask and Vaccine onload    
--]]
local function playerAdded(otherPart)
	--[[otherPart.RespawnLocation = game.Workspace.SpawnLocation
    if  otherPart.leaderstats ~= nil then
        otherPart.leaderstats.Mask.Value = 0;
        otherPart.leaderstats.Vaccine.Value = 0;
    end
    --]]
    
end
local Players = game:GetService("Players")
Players.PlayerAdded:Connect(playerAdded)
 
-- go through existing players
for _, player in pairs(Players:GetPlayers()) do 
	playerAdded(player)
end

generateVirus()
generateMaskPosition()
setupActionTouchIntoVirusModel()


