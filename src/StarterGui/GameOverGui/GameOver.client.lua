--[[
@description Trigger when player health equal 0 then show label Game Over
--]]
game.Players.LocalPlayer.Character:WaitForChild("Humanoid").Died:Connect(function()
	script.Parent.GameOver.Visible = true
end)