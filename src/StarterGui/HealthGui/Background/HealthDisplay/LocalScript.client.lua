--[[
@description change size of health bar when player hit virus
--]]
local player = game.Players.LocalPlayer
local char = player.Character or player.CharacterAdded:Wait()
local humanoid = char:WaitForChild("Humanoid")

humanoid.HealthChanged:Connect(function(damage)
    script.Parent.Size = UDim2.new(damage/humanoid.MaxHealth,0,1,0)
end)